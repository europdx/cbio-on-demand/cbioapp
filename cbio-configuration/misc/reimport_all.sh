#!/bin/bash
BASEDIR="$(find /data/cbio-data/europdx-data-public/ -maxdepth 0 -type d)"

for D in `find /data/cbio-data/europdx-data-public/ -maxdepth 1 -type d`
do
    # skip .git folder
    if [[ $D == *".git"* ]]; then
        continue;
    fi

    # skip base folder
    if [[ $(basename $D) == "$(basename $BASEDIR)" ]]; then
        continue;
    fi

    # docker exec cbioportal metaImport.py -s /cbio_data_ro/europdx-data-public/$(basename $D)/ -u http://localhost:8080/cbioportal -o;
    docker exec importer-container metaImport.py -u http://cbioportal-container:8080/cbioportal -s /study/europdx-data-public/$(basename $D)/ -o

done

docker restart cbioportal-container;

# print folder names - recapitulation
for D in `find /data/cbio-data/europdx-data-public/ -maxdepth 1 -type d`
do
    # skip .git folder
    if [[ $D == *".git"* ]]; then
        continue;
    fi

    # skip base folder
    if [[ $(basename $D) == "$(basename $BASEDIR)" ]]; then
        continue;
    fi

    echo "$(basename $D)"
done


